# PE format

PE32 Format image (from: [https://en.wikipedia.org/wiki/Portable_Executable](https://en.wikipedia.org/wiki/Portable_Executable)):

![](./pe32.svg)

# Thanks

Port from [https://github.com/golang/go/tree/master/src/debug/pe](https://github.com/golang/go/tree/master/src/debug/pe)